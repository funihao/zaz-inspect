ZAZ Inspect

Extension for instronspeccion objects UNO in LibreOffice.

Inspired by the great extension: [https://github.com/hanya/MRI](https://github.com/hanya/MRI)


Thanks!

https://gitlab.com/mauriciobaeza/zaz


### Software libre, no gratis


This extension have a cost of maintenance of 1 euro every year.

BCH: `qztd3l00xle5tffdqvh2snvadkuau2ml0uqm4n875d`

BTC: `3FhiXcXmAesmQzrNEngjHFnvaJRhU1AGWV`

PayPal :( donate ATT elmau DOT net


* [Look the wiki](https://gitlab.com/mauriciobaeza/zaz-inspect/wikis/home)
* [Mira la wiki](https://gitlab.com/mauriciobaeza/zaz-inspect/wikis/home_es)


Thanks for translations:

* Help Us
